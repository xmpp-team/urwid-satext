urwid-satext (0.9.0~hg151-1) unstable; urgency=medium

  * New upstream snapshot

 -- Martin <debacle@debian.org>  Fri, 03 Nov 2023 00:22:01 +0000

urwid-satext (0.9.0~hg150.aa8f46b43a71-1) unstable; urgency=medium

  * Add some minor fixes from upstream

 -- Martin <debacle@debian.org>  Mon, 16 Jan 2023 23:41:56 +0000

urwid-satext (0.8.0-1) unstable; urgency=medium

  * New upstream version

 -- Martin <debacle@debian.org>  Thu, 13 Jan 2022 18:46:39 +0000

urwid-satext (0.8.0~hg144.bfab04d0a745-1) unstable; urgency=medium

  * New upstream hg tip

 -- Martin <debacle@debian.org>  Thu, 02 Apr 2020 13:41:35 +0000

urwid-satext (0.8.0~hg143.144bdf877d21-1) unstable; urgency=medium

  * New upstream hg tip, change from Python 2 to Python 3

 -- W. Martin Borgert <debacle@debian.org>  Tue, 22 Oct 2019 23:30:36 +0000

urwid-satext (0.7.0.a2-1) unstable; urgency=medium

  * New upstream version

 -- W. Martin Borgert <debacle@debian.org>  Mon, 15 Oct 2018 20:39:53 +0000

urwid-satext (0.7.0d-1) unstable; urgency=medium

  [ Thomas Preud'homme  2016-08-07 ]
  * Update debian/copyright.

  [ W. Martin Borgert ]
  * New upstream version (Closes: #890304)
  * Move packaging to XMPP team and salsa.debian.org
  * Bump standards version and dh compat level
  * Add myself to uploaders

 -- W. Martin Borgert <debacle@debian.org>  Wed, 21 Feb 2018 20:56:21 +0000

urwid-satext (0.6.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + use encrypted URLs for Vcs-* fields
    + bump Standards-Version to 3.9.8 (no changes needed)
  * debian/source/lintian-overrides: add override for not checking GPG in
    debian/watch since upstream tarball do not have GPG signatures.
  * Remove generated files when cleaning.

 -- Thomas Preud'homme <robotux@debian.org>  Sun, 07 Aug 2016 16:29:20 +0100

urwid-satext (0.4.1-1) unstable; urgency=medium

  * New upstream release.
  * DEP-12: remove Homepage and Watch fields, which are not recommended.

 -- Matteo Cypriani <mcy@lm7.fr>  Sat, 20 Sep 2014 23:38:31 -0400

urwid-satext (0.4.0-1) unstable; urgency=medium

  * New upstream release.
    + Bump minimal version of urwid required.
    + Stop shipping a specific version of distribute_setup.py.
    + Lower the default version of python-setuptools in ez_setup.py.
  * debian/copyright: update and fix URI for relicensing.
  * Add debian/upstream/metadata (DEP-12).

 -- Matteo Cypriani <mcy@lm7.fr>  Sun, 14 Sep 2014 22:54:25 -0400

urwid-satext (0.3.0-1) unstable; urgency=medium

  * New upstream version.
    + Remove patch adapt_to_urwid_1_1.diff.

 -- Matteo Cypriani <mcy@lm7.fr>  Wed, 26 Feb 2014 13:23:02 -0500

urwid-satext (0.2.0-5) unstable; urgency=medium

  * Upload to unstable.
  * Also use newer distribute_setup.py for build target (Closes: #735793).

 -- Thomas Preud'homme <robotux@debian.org>  Thu, 13 Feb 2014 23:18:12 +0800

urwid-satext (0.2.0-4) experimental; urgency=medium

  * Use newer distribute_setup.py to work with recent python-setuptools
    (Closes: #735793).
  * Bump Standards-Version to 3.9.5 (no changes needed).

 -- Thomas Preud'homme <robotux@debian.org>  Tue, 11 Feb 2014 21:51:01 +0800

urwid-satext (0.2.0-3) unstable; urgency=low

  * Upload to unstable.

 -- Thomas Preud'homme <robotux@debian.org>  Mon, 24 Jun 2013 14:10:44 +0200

urwid-satext (0.2.0-2) experimental; urgency=low

  * Add patch to adapt urwid-satext to build with urwid 1.1 API.

 -- Thomas Preud'homme <robotux@debian.org>  Fri, 07 Jun 2013 15:19:09 +0200

urwid-satext (0.2.0-1) experimental; urgency=low

  * Initial release. (Closes: #703917)

 -- Thomas Preud'homme <robotux@debian.org>  Wed, 15 May 2013 16:14:34 +0200
